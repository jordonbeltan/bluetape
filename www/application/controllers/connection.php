<?php

class MySQLDB
{
    protected $servername = "localhost";
    protected $username = "admin";
    protected $password = "localhost";
    protected $dbname = "bluetape";

    protected $db_connection;

    function openConnection()
    {
        // Create connection
        $this->db_connection = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

        // Check connection
        if ($this->db_connection->connect_error) {
            die('Could not connect to ' . $this->servername . ' server');
        }
    }

    function executeQuery($sql)
    {
        $this->openConnection();
        $query_result = $this->db_connection->query($sql);
        $result = [];
        if ($query_result->num_rows > 0) {
            // Output data of each row
            while ($row = $query_result->fetch_array()) {
                $result[] = $row;
            }
        }
        $this->db_connection->close();
        return $result;
    }

    function executeNonQuery($sql)
    {
        $this->openConnection();
        if ($this->db_connection->query($sql) == false) {
            return false;
        }
        $this->db_connection->close();

        return true;
    }
}